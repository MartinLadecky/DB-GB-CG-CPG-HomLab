function [val] = scalar_product(a,b)

val=sum(sum(conj(a).*b));
end