function [c_1,st,norm_evol_rr,norm_evol_energy,norm_evol_grad, norm_sol,rel_sol_error]...
    = solver_DB_PCG(A,G,c_0,E,M,steps,toler,ref_solution)
    %% input
    M=M.^-1;
    M((end+1)/2,(end+1)/2)=0;
    
    norm_sol(1)=sqrt(scalar_product_grad_energy(G.*c_0,G.*c_0,A));

    ref_sol_norm=sqrt(scalar_product_grad(ref_solution,ref_solution));
    sol_error=sqrt(scalar_product_grad(ref_solution-G.*c_0,ref_solution-G.*c_0));
    rel_sol_error(1)=sol_error/ref_sol_norm;


    Kx_0 = K_stiffness_freq(A,c_0,G); 
    b_0 = -RHS_freq(A,E,G);  

    r_0 = b_0-Kx_0; 
    
    nr0 =sqrt(scalar_product(r_0,r_0));
    norm_evol_rr(1)=nr0/nr0;
    
    nDr0Dr0=sqrt(scalar_product_grad(G.*r_0,G.*r_0));
    norm_evol_grad(1)=nDr0Dr0/nDr0Dr0;

    z_0 = r_0.*M;
    
    scalar_product(r_0,r_0);
    scalar_product(r_0,z_0);
    
    nz0r0 = sqrt(scalar_product(r_0,z_0));
    norm_evol_energy(1)=nz0r0/nz0r0;
    
    p_0 = z_0;

    for st = 1:steps
        Ap_0 = K_stiffness_freq(A,p_0,G);
     
        r_0z_0= scalar_product(r_0,z_0 );
        p_0Ap_0=scalar_product(p_0,Ap_0);
        alfa_0 = r_0z_0/p_0Ap_0;
        
        c_1 = c_0 + alfa_0.*p_0;
        
        norm_sol(st+1)=sqrt(scalar_product_grad_energy(G.*c_1,G.*c_1,A));
        sol_error=sqrt(scalar_product_grad(ref_solution- G.*c_1,ref_solution-G.*c_1));
        rel_sol_error(st+1)=sol_error/ref_sol_norm;

        r_1 = r_0-alfa_0*Ap_0;

        
        nr1 =sqrt(scalar_product(r_1,r_1 ));
        norm_evol_rr(st+1)=nr1/nr0;

        
        z_1=r_1.*M;
        scalar_product(r_1,r_1 );
        r_1z_1=scalar_product(r_1,z_1 );
        norm_evol_energy(st+1)=sqrt(r_1z_1)/nz0r0;
        
        
        nDr1Dr1=sqrt(scalar_product_grad(G.*r_1,G.*r_1));
        norm_evol_grad(st+1)=nDr1Dr1/nDr0Dr0;

            if ( norm_evol_rr(st+1)<toler)
                break; 
            end  

        beta_1 =r_1z_1 /r_0z_0;
        p_1 = z_1 + beta_1*p_0;
        %% reiniticialisation
       
        p_0 = p_1;
        r_0 = r_1;
        z_0 = z_1; 
        c_0 = c_1;

    end
end






