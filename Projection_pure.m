function [GPAFGFx] = Projection_pure(GFx,G)
% DtID
G_h=conj(G);

M=(G_h(:,:,1).*(G(:,:,1))+G_h(:,:,2).*(G(:,:,2)));  
M((end+1)/2,(end+1)/2)=1;


%% Projection operator

GFAFGFx_p=conj(G).*GFx;
GFAFGFx=GFAFGFx_p(:,:,1)+GFAFGFx_p(:,:,2);

GC_refG_inv=M.^-1;
GC_refG_inv((end+1)/2,(end+1)/2)=0;


GPAFGFx=G.*GC_refG_inv.*GFAFGFx;
GPAFGFx((end+1)/2,(end+1)/2,:)=0;

end