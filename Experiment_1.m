
%% BEGIN
clc;clear;

counter=1;
steps = 500;    
for loop=[10]
N_1=2*(loop^2)+1

N_2=N_1; % number of points in x_2

%% Mesh parameters
h_1=2*pi/(N_1); % step in x_2
h_2=2*pi/(N_2); % step in x_2

%% Coordinates
x=zeros(N_2,N_1,2);
[x(:,:,1),x(:,:,2)]=meshgrid(-pi+h_1/2:h_1:pi-h_1/2,-pi+h_2/2:h_2:pi-h_2/2);

%% Material coeficient matrix

C=zeros(N_2,N_1,2,2);
center=idivide(N_1,int8(2));
half=center/2;

phase_1=[ 1, 0 ;...
          0, 1 ];
phase_1_eigens=eig(phase_1)

phase_2=[ 1,  2;...
          2, 10];
phase_2_eigens=eig(phase_2)

for i=1:N_2
    for j=1:N_1
        if (abs(center-i)+abs(center-j)) > half
            C(i,j,:,:)=phase_1;
                 
        else 
            C(i,j,:,:)=phase_2; % inclusion   

        end      
    end       
end

%% Material ananlysis: reference material
 
%  d=[mean(mean(C(:,:,1,1))) mean(mean(C(:,:,1,2)));...
%    mean(mean(C(:,:,2,1))) mean(mean(C(:,:,2,2)))]
%  d=[1 0;
%     0 1];
  d=[1 0;
     0 10];
 
 
Ref_Mat_eigens=eig(d)

C_ref=zeros(N_2,N_1,2,2); 
C_ref_inv=zeros(N_2,N_1,2,2); 
    for i=1:N_2
        for j=1:N_1    
            C_ref(i,j,:,:)=d;
            C_ref_inv(i,j,:,:)=d^-1;
        end
    end

%% Gradient operator
G = G_clasic(N_1,N_2);

%% Preconditioner

G_h=conj(G);

M=(G_h(:,:,1).*d(1,1).*(G(:,:,1))+G_h(:,:,2).*d(2,2).*(G(:,:,2))...
                   +d(1,2).*(G_h(:,:,1).*(G(:,:,2)))+d(2,1).*(G(:,:,1).*(G_h(:,:,2))));  
M((end+1)/2,(end+1)/2)=1;

%% Conditions:


c_0=zeros(N_2,N_1);
E_0=[1 0;0 1];
A_0=zeros(2,2);

E=E_0(:,1);


%% Displacement-Based Preconditioned Conjugage Gradient solver
disp('REFERENCE SOLUTION Displacement-Based Preconditioned Conjugage Gradient solver')

toler = 1e-10;
    init_ref_sol=zeros(N_1,N_2,2);
    [C_DB_PCG,~]...
        =solver_DB_PCG(C,G,c_0,E,M,steps,toler,init_ref_sol);% with preconditioning

    %S_DB_PCG(1,counter) = st_DB_PCG+1;
    
    %A_(:,1)=Hom_parameter(C_DB_PCG,C,G,E)% Compute homogenized parameter
    %A_DB_PCG(counter)=A_(1,1);  

    
    ref_solution=G.*C_DB_PCG;
    
    
%% Displacement-Based Preconditioned Conjugage Gradient solver
disp('Displacement-Based Preconditioned Conjugage Gradient solver')

toler = 1e-6;

    [C_DB_PCG,st_DB_PCG,norm_evol_DB_PCG_rr,norm_evol_DB_PCG_energy, norm_evol_DB_PCG_grad,sol_norm_DB_PCG,rel_sol_error_DB_PCG]...
        =solver_DB_PCG(C,G,c_0,E,M,steps,toler,ref_solution);% with preconditioning

    S_DB_PCG(1,counter) = st_DB_PCG+1;
    
    A_(:,1)=Hom_parameter(C_DB_PCG,C,G,E)% Compute homogenized parameter
    A_DB_PCG(counter)=A_(1,1);  

    GC_DB_PCG=G.*C_DB_PCG;
    
%% Gradient-Based solvers

disp('Gradient-Based Conjugage Gradient solver')

   [C_GB_CG,st_GB_CG, norm_evol_GB_CG_grad, norm_evol_GB_CG_energy,   sol_norm_GB_CG,rel_sol_error_GB_CG]...
       =solver_GB_CG(C,G,c_0,E,M,d,steps,toler,ref_solution);

    S_GB_CG(1,counter) = st_GB_CG+1;
    
    A_(:,1)=Hom_parameter(C_GB_CG,C,G,E) % Compute homogenized parameter
    A_GB_CG(counter)=A_(1,1);


    
disp('Modifield Gradient-Based Conjugage Gradient solver')


    [C_GB_CG_mod,st_GB_CG_mod,norm_evol_GB_CG_mod_grad,norm_evol_GB_CG_mod_energy,   sol_norm_GB_CG_mod,rel_sol_error_GB_CG_mod]...
        =solver_GB_CG_modif(C,G,c_0,E,M,d,steps,toler,ref_solution);

    S_GB_CG_mod(1,counter) =st_GB_CG_mod+1;

    A_(:,1)=Hom_parameter(C_GB_CG_mod,C,G,E) % Compute homogenized parameter
    A_GB_CG_mod(counter)=A_(1,1);


disp('Gradient-Based Preconditioned Conjugage Gradient solver')


    [C_GB_PCG,st_GB_PCG, norm_evol_GB_PCG_rr, norm_evol_GB_PCG_energy, sol_norm_GB_PCG,rel_sol_error_GB_PCG]...
     =solver_GB_PCG(C,G,c_0,E,M,steps,toler,ref_solution);


    S_GB_PCG(1,counter) = st_GB_PCG+1;

    A_(:,1)=Hom_parameter(C_GB_PCG,C,G,E) % Compute homogenized parameter
    A_GB_PCG(counter)=A_(1,1);

disp('Gradient-Based Preconditioned Conjugage Gradient solver Symmetric')


    [C_GB_PCG_s,st_GB_PCG_s, norm_evol_GB_PCG_s_rr, norm_evol_GB_PCG_s_energy, sol_norm_GB_PCG_s,rel_sol_error_GB_PCG_s]...
     =solver_GB_PCG_symmetric(C,G,c_0,E,M,steps,toler,ref_solution);


    S_GB_PCG_s(1,counter) = st_GB_PCG_s+1;

    A_(:,1)=Hom_parameter(C_GB_PCG_s,C,G,E) % Compute homogenized parameter
    A_GB_PCG_s(counter)=A_(1,1);

NoP(counter)=N_1*N_2;
counter=counter+1;
end

disp('TEST: is solution projection invariant?')

C_GB_CG_projected=Projection_pure(C_GB_CG,G) ;
PC_GB_CG=sum(sum(sum(C_GB_CG_projected-C_GB_CG)))

C_GB_CG_projected2=Projection_pure(C_GB_CG_projected,G) ;
PPC_GB_CG=sum(sum(sum(C_GB_CG_projected-C_GB_CG_projected2)))

C_DB_PCG_projected=Projection_pure(G.*C_DB_PCG,G) ;
PC_DB_PCG=sum(sum(sum(C_DB_PCG_projected-G.*C_DB_PCG)))

C_DB_PCG_projected2=Projection_pure(C_DB_PCG_projected,G) ;
PPC_DB_PCG=sum(sum(sum(C_DB_PCG_projected2-C_DB_PCG_projected)))

%% Plot residuals

%close all
figure 
hold on

    plot((1:S_DB_PCG(1,end)),norm_evol_DB_PCG_rr,'--xb')
    plot((1:S_DB_PCG(1,end)),real(norm_evol_DB_PCG_energy),'--ob')

    plot((1:S_GB_CG(1,end)),norm_evol_GB_CG_grad,'-.xr')
    plot((1:S_GB_CG(1,end)),real(norm_evol_GB_CG_energy),'-.or')

    plot((1:S_GB_CG_mod(1,end)),norm_evol_GB_CG_mod_grad,'-.xg')
    plot((1:S_GB_CG_mod(1,end)),real(norm_evol_GB_CG_mod_energy),'-.og')

    plot((1:S_GB_PCG(1,end)),norm_evol_GB_PCG_rr,'-.xk')
    plot((1:S_GB_PCG(1,end)),real(norm_evol_GB_PCG_energy),'-.ok')
    
        plot((1:S_GB_PCG_s(1,end)),norm_evol_GB_PCG_s_rr,'-.+m')
    plot((1:S_GB_PCG_s(1,end)),real(norm_evol_GB_PCG_s_energy),'-.>m')

set(gca, 'XScale', 'linear', 'YScale', 'log');
legend( 'DB PCG || r ||',       'DB PCG || r ||_M', ...
        'GB CG || r^* ||',      'GB CG || r^* ||_M',...
        'GB CG mod || r^{m} ||','GB CG mod || r ||_M',...
        'GB PCG || r^{@} ||',   'GB PCG || r ||_M',...
        'GB PCG_s || r^{@s} ||',   'GB PCG_s || r ||_M') %'DB PCG || Dr ||',
title('Residuals ')
figure 
hold on

    plot((1:S_DB_PCG(1,end)),norm_evol_DB_PCG_rr,'--xb')
    plot((1:S_DB_PCG(1,end)),norm_evol_DB_PCG_energy,'--ob')

    plot((1:S_GB_CG(1,end)),norm_evol_GB_CG_grad,'-.<r')
    plot((1:S_GB_CG(1,end)),norm_evol_GB_CG_energy,'-.^r')

    plot((1:S_GB_CG_mod(1,end)),norm_evol_GB_CG_mod_grad,'-.xg')
    plot((1:S_GB_CG_mod(1,end)),norm_evol_GB_CG_mod_energy,'-.og')

    plot((1:S_GB_PCG(1,end)),norm_evol_GB_PCG_rr,'-.xk')
    plot((1:S_GB_PCG(1,end)),norm_evol_GB_PCG_energy,'-.ok')

%set(gca, 'XScale', 'linear', 'YScale', 'log');
symlog(gca,'xy',-1)
legend( 'DB PCG || r ||',       'DB PCG || r ||_M', ...
        'GB CG || r^* ||',      'GB CG || r^* ||_M',...
        'GB CG mod || r^{m} ||','GB CG mod || r ||_M',...
        'GB PCG || r^{@} ||',   'GB PCG || r ||_M') %'DB PCG || Dr ||',
title('Residuals ')
%% Plot solution norm
figure 
hold on


    plot((1:S_DB_PCG(1,end)),abs(sol_norm_DB_PCG(end)-sol_norm_DB_PCG),'-.^')
    plot((1:S_GB_CG(1,end)),abs(sol_norm_DB_PCG(end)-sol_norm_GB_CG),'-.*')

    plot((1:S_GB_CG_mod(1,end)),abs(sol_norm_DB_PCG(end)-sol_norm_GB_CG_mod),'-.o')
  
    plot((1:S_GB_PCG(1,end)),abs(sol_norm_DB_PCG(end)-sol_norm_GB_PCG),'-.^')
 
set(gca, 'XScale', 'linear', 'YScale', 'log');
legend('DB PCG - GB CG || Du ||_C',...
       'DB PCG || Du ||_C','GB CG || Du ||_C',...
       'GB mCG || Du ||_C','GB PCG || Du ||_C')
title('solution norm: norm(end)-norm')
%% Plot solution norm
figure 
hold on
    plot((1:S_GB_CG(1,end)),rel_sol_error_GB_CG,'-.^')
    plot((1:S_GB_CG_mod(1,end)),rel_sol_error_GB_CG_mod,'-.*')
    plot((1:S_GB_PCG(1,end)),rel_sol_error_GB_PCG,'-.o')
    plot((1:S_GB_PCG_s(1,end)),rel_sol_error_GB_PCG_s,'-.s')
    plot((1:S_DB_PCG(1,end)),rel_sol_error_DB_PCG,'-.+')

set(gca, 'XScale', 'linear', 'YScale', 'log');
legend('ref - GB CG || Du ||','ref - GB mCG || Du ||',...
       'ref - GB PCG || Du ||','ref - GB PCG_s || Du ||',...
       'ref - DB PCG || Du ||')
title('solution norm: ||ref-x_i||/||ref||')

%% Plot solution norm
figure 
hold on

    plot((1:S_DB_PCG(1,end)),sol_norm_DB_PCG,'-.^')
    plot((1:S_GB_CG(1,end)),sol_norm_GB_CG,'-.*')

    plot((1:S_GB_CG_mod(1,end)),sol_norm_GB_CG_mod,'-.o')

    plot((1:S_GB_PCG(1,end)),sol_norm_GB_PCG,'-.^')
 
set(gca, 'XScale', 'linear', 'YScale', 'log');
legend('DB PCG ','GB CG ','GB CG modif','GB PCG ')
title('solution norm: absolut')




 %% Plot A function
 subplot(2,2,1);
    mesh(C(:,:,1,1))
subplot(2,2,2);
    mesh(C(:,:,1,2))
subplot(2,2,3);
    mesh(C(:,:,2,1))
subplot(2,2,4);
    mesh(C(:,:,2,2))